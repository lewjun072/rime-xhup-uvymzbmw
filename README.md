# rime-xhup-uvymzbmw

## 介绍
rime小鹤双拼配置方案

## 下载RIME
可以从RIME的[下載及安裝](https://rime.im/download/)页面下载对应平台的软件
* Windows：小狼毫（Weasel）
* macOS：鼠鬚管（Squirrel）
* Android：同文（Trime）

## 项目来源于小鹤音形“鼠须管”for macOS.zip
[小鹤音形“鼠须管”for macOS.zip](http://ys-l.ysepan.com/116124357/319356963/VISkjKj8J3I5L6159PL4c1/%E5%B0%8F%E9%B9%A4%E9%9F%B3%E5%BD%A2%E2%80%9C%E9%BC%A0%E9%A1%BB%E7%AE%A1%E2%80%9Dfor%20macOS.zip?lx=xz)适用于macOS：鼠鬚管（Squirrel），那么也就适用于RIME

这些文件不能删
```bash
C:\小鹤音形Rime平台鼠须管for macOS\rime
λ tree /f
C:.
│  default.custom.yaml
│  default.yaml
│  flypy.schema.yaml
│  flypy_full全码字.txt
│  flypy_sys.txt
│  flypy_top.txt
│  flypy_user.txt
│  rime.lua
│  squirrel.custom.yaml
│  squirrel.yaml
│
├─build
│      flypy.prism.bin
│      flypy.reverse.bin
│      flypy.table.bin
│      flypydz.prism.bin
│      flypydz.reverse.bin
│      flypydz.table.bin
│
└─lua
        calculator_translator.lua
```

之前误删`default.custom.yaml`文件，导致Trime选择当前方案不能勾选，启用方案失败。

## 主题文件

目录下类似`flypy.*.trime.yaml`的文件，都是flypy的主题文件，来源于flypyTR_Android_10.9.4.apk
```
flypy.＃曲悠扬＃.trime.yaml
flypy.小鹤展翅.trime.yaml
flypy.比翼.trime.yaml
flypy.比翼高飞.trime.yaml
flypy.霓裳.trime.yaml
flypy.霓裳风起.trime.yaml
```

## git clone并部署
删除目录前，如果有重要数据，一定要先备份。

* Windows：小狼毫（Weasel） 
```bash
退出算法服务
cd D:\Program Files\Rime\
rm DataDir
git clone https://gitee.com/lewjun072/rime-xhup-uvymzbmw.git DataDir
重启算法服务
```


* macOS：鼠鬚管（Squirrel）
```sh
huiye$ cd ~/Library/
huiye$ mv Rime Rime.bak
huiye$ git clone https://gitee.com/lewjun072/rime-xhup-uvymzbmw.git Rime

Deploy

huiye$ rm -rf Rime.bak
```


* Android：同文（Trime）

方式1：
```sh
# 删除原来的rime目录
rm -rf rime

git clone https://gitee.com/lewjun072/rime-xhup-uvymzbmw.git rime
```

方式2：
```sh
# 删除原来的rime目录
rm -rf rime
# 重新部署生成rime目录
部署
# git 下载到rime-git目录
git clone https://gitee.com/lewjun072/rime-xhup-uvymzbmw.git rime-git
# 将rime-git/下的内容复制到rime/
mv rime-git/* rime/
# 将rime-git/下的.git/ 以及 .gitignore 复制到rime/目录
mv rime-git/.[^.]* rime/
rm -rf rime-git
```


重新部署，选择主题和与配色。


## 使用报错解决方案

* 在PC端当修改了文件`build/flypy.prism.bin`后，暂存或丢弃或`git rebase`都有可能报错。这是因为该文件被`小狼豪算法服务`占用了。
    - 解决方式1：退出算法服务，再做git相关操作，最后记得`重启算法服务`，重新部署。。
    - 解决方式2：打开`任务管理器`，找到并结束任务`小狼豪算法服务`，再做git相关操作，最后记得`重启算法服务`，重新部署。

* git reset

在手机上配置同文输入法时，常常会因为格式不对（Tab或空格或缩进），导致方案不可选，这时就可以使用`git reset`命令来还原到某一次提交就解决这个问题了。

```shell
λ git log
commit 3bf5f7f0b524147e5806a5435d45c8c857db7f05 (HEAD -> master, origin/master, origin/HEAD)
Author: LewJun <lewjun072@qq.com>
Date:   Mon Jul 8 09:18:32 2024 +0800

    添加自定义词

commit f2e7c565eccd597730c31c622d696b74cc6b64f6
Author: lewjun <lewjun072@qq.com>
Date:   Mon Jul 8 00:00:38 2024 +0800

    修改状态栏上显示的内容，以及菜单中添加候选窗开关

commit bd9719c6210aa803e89a593a8ca04c0823e4bf91
Author: lewjun <lewjun072@qq.com>
Date:   Sun Jul 7 16:41:04 2024 +0800

    使用tongwen的配置方式解决候选换行的问题 flypy.trime.yaml

# 还原到指定的提交点
λ git reset --hard 3bf5
# 或者还原到最近一次提交点
git reset --hard HEAD~1
```

--hard: 移动 HEAD 指针并重置索引和工作区，彻底删除了提交以及暂存区和工作区的修改，慎用，<font color="red">使用前最好备份下目录，因为会导致工作区的内容丢失</font>。
git reset --hard [commit]

--mixed 或不带选项（默认）：移动 HEAD 指针并重置索引，不会修改工作区，撤销了提交和暂存的更改，但保留了工作区的修改。
git reset [commit]

--soft: 只移动HEAD指针，暂存区和工作目录中的更改都会保留在工作目录中，以便再次提交。
git reset --soft [commit]







## 结束
