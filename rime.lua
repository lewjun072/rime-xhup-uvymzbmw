function date_translator(input, seg)
    if (input == "orq") then
        --- Candidate(type, start, end, text, comment)
        yield(Candidate("date", seg.start, seg._end, os.date("%Y/%m/%d"), ""))
        yield(Candidate("date", seg.start, seg._end, os.date("%Y年%m月%d日"), ""))



        --星期
        local day_w=os.date("%w")
        local day_w1=""
        if day_w=="0" then day_w1="周日" end
        if day_w=="1" then day_w1="周一" end
        if day_w=="2" then day_w1="周二" end
        if day_w=="3" then day_w1="周三" end
        if day_w=="4" then day_w1="周四" end
        if day_w=="5" then day_w1="周五" end
        if day_w=="6" then day_w1="周六" end

        yield(Candidate("date", seg.start, seg._end, os.date("%Y-%m-%d").." "..tostring(day_w1), " "))
    end
end

function time_translator(input, seg)
    if (input == "ouj") then
        local cand1 = Candidate("time", seg.start, seg._end, os.date("%H时%M分%S秒"), " ")
        cand1.quality = 1
        yield(cand1)

        local cand2 = Candidate("time", seg.start, seg._end, os.date("%H:%M:%S"), " ")
        cand2.quality = 2
        yield(cand2)
    end
end

function uji_translator(input, seg)
    if (input == "ouji") then
        -- 获取当前时间戳(秒)
        local timestamp = os.time()
        -- 获取当前时间戳的毫秒部分
        local milliseconds = math.floor(os.clock() * 1000) % 1000
        local full_timestamp = timestamp * 1000 + milliseconds

        -- 毫秒 11时42分46秒 11:42:48 1739418172906
        local cand1 = Candidate("uji", seg.start, seg._end, full_timestamp, "")
        cand1.quality = 1
        yield(cand1)

        -- 秒
        local cand3 = Candidate("uji", seg.start, seg._end, timestamp, "")
        cand3.quality = 2
        yield(cand3)
    end
end

function uid_translator(input, seg)
    if (input == 'ouid') then
        local uid = generateUUID()
        local cand = Candidate("uid", seg.start, seg._end, uid, "")
        cand.quality = 1
        yield(cand)
    end
end

function generateUUID()
    local template = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
    return string.gsub(template, '[xy]', function (c)
        local v = (c == 'x') and math.random(0, 0xf) or math.random(8, 0xb)
        return string.format('%x', v)
    end)
end


function exe_translator(input, seg)
    if (input == 'oju') then
        -- local cand = Candidate('oju', seg.start, seg._end, '[记事本]', '')
        -- cand.quality = 1
        -- yield(cand)
        -- return 1 -- 表示已经处理了这个输入，不再需要其他翻译器处理

        os.execute("start /b notepad.exe")
        return true
    end

    if (input == 'oht') then
        os.execute("start /b mspaint.exe")
        return true
    end

    if (input == 'odn') then
        os.execute("start /b explorer.exe")
        return true
    end

    if (input == 'ojs') then
        os.execute("start /b calc.exe")
        return true
    end

    if (input == 'ocmd') then
        os.execute("start /b cmd.exe")
        return true
    end


end


calculator_translator = require("calculator_translator")

-- TODO 自定义处理器不知道该怎么配置解析器
-- function exe_processor(input, seg, candidate)
--     print(candidate.type)
--     if (candidate.type == 'oju') then
--         os.execute("start /b notepad.exe")
--         return 1
--     end
-- end
